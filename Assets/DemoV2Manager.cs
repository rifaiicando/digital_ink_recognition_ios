﻿//
// Fingers Gestures
// (c) 2015 Digital Ruby, LLC
// http://www.digitalruby.com
// Source code may be used for personal or commercial projects.
// Source code may NOT be redistributed or sold.
// 

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DigitalRubyShared;

public class DemoV2Manager : MonoBehaviour
{
    [SerializeField] Texture2D defaultBackgroundTexture;
    [SerializeField] Text resultText;
    [SerializeField] Image Background;
    [SerializeField] GameObject ui;
    [SerializeField] FingersImageGestureHelperComponentScript ImageScript;

    [SerializeField] List<ButtonItemRecognition> listButtonText;
    

    private void LinesUpdated(object sender, System.EventArgs args)
    {
        // Debug.LogFormat("Lines updated, new point: {0},{1}", ImageScript.Gesture.FocusX, ImageScript.Gesture.FocusY);
    }

    private void LinesCleared(object sender, System.EventArgs args)
    {
        // Debug.LogFormat("Lines cleared!");
    }

    private void Start()
    {
        resultText.text = "";
        for (int i = 0; i < listButtonText.Count; i++)
        {
            listButtonText[i].manager = this;
            listButtonText[i].position = i;
            listButtonText[i].result = "-";
        }

        ImageScript.LinesUpdated += LinesUpdated;
        ImageScript.LinesCleared += LinesCleared;
    }

    public void Read(Action<string> result)
    {
        StartCoroutine(GetTexture((tex)=>
        {
            ImageGestureImage match = ImageScript.MatchedImage;
            string resultString = "-";
            if (match != null)
            {
                resultString = match.Name;
            }
            Background.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));
            
            Debug.Log(resultString);
            result.Invoke(resultString);
        }));
    }

    public void ResetRecognition()
    {
        ImageScript.Reset();
    }

    public void Clear()
    {
        resultText.text = "";
        for (int i = 0; i < listButtonText.Count; i++)
        {
            listButtonText[i].manager = this;
            listButtonText[i].position = i;
            listButtonText[i].result = "-";
        }
        Background.sprite = Sprite.Create(defaultBackgroundTexture, new Rect(0, 0, defaultBackgroundTexture.width, defaultBackgroundTexture.height), new Vector2(0.5f, 0.5f));
        ImageScript.Reset();
    }

    public void ReadAll()
    {
        string result = "";

        for (int i = 0; i < listButtonText.Count; i++)
        {
            result += listButtonText[i].result;
        }
        Debug.Log(result);
        resultText.text = result;
        
    }

    private IEnumerator GetTexture(Action<Texture2D> callback)
    {
        // ui.SetActive(false);
        yield return new WaitForEndOfFrame();

        Texture2D tex = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        tex.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        tex.Apply();

        // ui.SetActive(true);
        
        callback(tex);
    }
}