﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.IO;
using System.Collections;

public class IOSHandwritingManager : MonoBehaviour
{
    [SerializeField] Button initButton;
    [SerializeField] Button generateButton;
    [SerializeField] Button clearButton;
    [SerializeField] Text resultText;
    [SerializeField] EventTrigger eventTrigger;

    HandwriteRecognitionIOSPluguin handwriteRecognition;




    public const string FILENAME = "Tulisan.png";

    public Image imageCanvas;

    public Color eraseColor = Color.white;

    protected TextureDrawingAux textureDrawingAux;
    protected Mode mode = Mode.Pencil;
    int drawingSize = 5;
    protected Color drawingColor = Color.black;
    
    Vector2 pencilFrom;
    
    Vector2 pencilTo;
    
    bool drawPencil;
    
    Texture2D stickerTexture;


    protected virtual IEnumerator Start()
    {
        eventTrigger.AddListener(EventTriggerType.PointerDown, OnPointerDown);
        eventTrigger.AddListener(EventTriggerType.Drag, OnPointerDrag);
        eventTrigger.AddListener(EventTriggerType.PointerUp, OnPointerUp);

        initButton.onClick.AddListener(()=>
        {
            #if (UNITY_IOS)
            if (HandwriteRecognitionIOSPluguin.Instance == null) 
            {
                HandwriteRecognitionIOSPluguin.Initialize();
                handwriteRecognition = HandwriteRecognitionIOSPluguin.Instance;
                SetText("Initializing");
                
                handwriteRecognition.OnCompleteInitialize += (status) => 
                {
                    if(status)
                        SetText("Initialization success");
                    else
                        SetText("Initialization failed");
                };
            }
            #endif
        });

        generateButton.onClick.AddListener(()=>
        {
            #if (UNITY_IOS)
            if (handwriteRecognition == null) 
            {
                SetText("Please Initialization");
            }
            else
            {
                handwriteRecognition.Generate((result)=>
                {
                    Debug.Log(result);
                    SetText(result);
                });
            }
            #endif
        });

        clearButton.onClick.AddListener(()=>
        {
            ClearCanvas();
            SetText("<<CLEAR>>");
            #if (UNITY_IOS)
            handwriteRecognition.Clear();
            #endif
        });





        int width = (int)imageCanvas.rectTransform.rect.width;
        int height = (int)imageCanvas.rectTransform.rect.height;
        textureDrawingAux = new TextureDrawingAux(width, height,1);

        textureDrawingAux.Clear(eraseColor);
        Sprite sprite = Sprite.Create(textureDrawingAux.Texture, new Rect(0, 0, textureDrawingAux.Width, textureDrawingAux.Height), Vector2.one * 0.5f);
        imageCanvas.sprite = sprite;

        yield return null;

        if (System.IO.File.Exists(Application.persistentDataPath + "/" + FILENAME))
        {
            byte[] bytes = File.ReadAllBytes(Application.persistentDataPath + "/" + FILENAME);

            Texture2D texture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
            texture.LoadImage(bytes);

            textureDrawingAux.DrawTexture(textureDrawingAux.Width/2-texture.width/2, textureDrawingAux.Height / 2 - texture.height / 2, texture);
        }

    }

    protected virtual void Update()
    {
        if (drawPencil)
        {
            Color color = mode == Mode.Eraser ? eraseColor : drawingColor;
            drawPencil = false;
            textureDrawingAux.DrawPencil(pencilFrom, pencilTo, drawingSize, color, mode == Mode.Eraser);
        }
    }
    
    public virtual void TogglePencilWithSize(int size)
    {
        mode = Mode.Pencil;
        drawingSize = size;
    }

    public virtual void ToggleSticker(Texture2D sticker)
    {
        mode = Mode.Sticker;
        this.stickerTexture = sticker;
    }
    
    public virtual void ToggleEraserWithSize(int size)
    {
        mode = Mode.Eraser;
        drawingSize = size;
    }
    
    public virtual void SetColor(Color color)
    {
        drawingColor = color;
    }


    public virtual void ClearCanvas()
    {
        textureDrawingAux.Clear(eraseColor);
    }
    
    public virtual void SaveToFile()
    {
        textureDrawingAux.SaveToFile(FILENAME);
    }
    
    public virtual void OnPointerDown(BaseEventData eventData)
    {
        PointerEventData pointerData = eventData as PointerEventData;
        
        Vector2 localCursor;
        if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(imageCanvas.rectTransform, pointerData.position, pointerData.pressEventCamera, out localCursor))
            return;
        localCursor.x += imageCanvas.rectTransform.pivot.x * imageCanvas.rectTransform.rect.width;
        localCursor.y += imageCanvas.rectTransform.pivot.y * imageCanvas.rectTransform.rect.height;

        if (mode == Mode.Sticker)
        {
            if (stickerTexture == null)
            {
                Debug.LogError("Sticker texture not set!");
                return;
            }
            textureDrawingAux.DrawTexture(Mathf.RoundToInt(localCursor.x)-stickerTexture.width/2, Mathf.RoundToInt(localCursor.y) - stickerTexture.height / 2, stickerTexture);
            return;
        }
        
        Color color = mode == Mode.Eraser ? eraseColor : drawingColor;
        
        textureDrawingAux.DrawCircleFill(Mathf.RoundToInt(localCursor.x), Mathf.RoundToInt(localCursor.y), drawingSize, color, mode == Mode.Eraser);

        
        #if (UNITY_IOS)
        if (HandwriteRecognitionIOSPluguin.Instance != null) 
        {
            HandwriteRecognitionIOSPluguin.Instance.BeginTouch(pointerData);
        }
        #endif
    }


    public virtual void OnPointerDrag(BaseEventData eventData)
    {
        if (mode == Mode.Sticker)
        {
            return;
        }
        PointerEventData pointerData = eventData as PointerEventData;
        
        Vector2 crtPos;
        if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(imageCanvas.rectTransform, pointerData.position, pointerData.pressEventCamera, out crtPos))
            return;
        Vector2 prevPos;
        if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(imageCanvas.rectTransform, pointerData.position - pointerData.delta, pointerData.pressEventCamera, out prevPos))
            return;
            
        pencilFrom = prevPos;
        pencilTo = crtPos;

        pencilFrom.x += imageCanvas.rectTransform.pivot.x * imageCanvas.rectTransform.rect.width;
        pencilFrom.y += imageCanvas.rectTransform.pivot.y * imageCanvas.rectTransform.rect.height;

        pencilTo.x += imageCanvas.rectTransform.pivot.x * imageCanvas.rectTransform.rect.width;
        pencilTo.y += imageCanvas.rectTransform.pivot.y * imageCanvas.rectTransform.rect.height;

        drawPencil = true;
        
        #if (UNITY_IOS)
        if (HandwriteRecognitionIOSPluguin.Instance != null) 
        {
            HandwriteRecognitionIOSPluguin.Instance.OnDrag(pointerData);
        }
        #endif
    }
    public virtual void OnPointerUp(BaseEventData eventData) 
    {

        PointerEventData pointerData = eventData as PointerEventData;
        
        #if (UNITY_IOS)
        if (HandwriteRecognitionIOSPluguin.Instance != null) 
        {
            HandwriteRecognitionIOSPluguin.Instance.EndTouch(pointerData);
        }
        #endif
    }

    private void SetText(string value)
    {
        resultText.text = value;
    }
    
    public enum Mode
    {
        //Set pixels traced over to set color
        Pencil = 0,
        //Set pixels traced over to white
        Eraser = 1,
        //Draw the selected sticker
        Sticker = 2
    }
}
