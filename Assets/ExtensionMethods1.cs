﻿using System;
using UnityEngine.EventSystems;

public static class ExtensionMethods1
{
    public static void AddListener(this EventTrigger trigger, EventTriggerType eventType, Action<PointerEventData> listener) {

        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener((data) => { listener((PointerEventData)data); });
        trigger.triggers.Add(entry);
    }
}
