﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.InteropServices;
using UnityEngine.EventSystems;

public class HandwriteRecognitionIOSPluguin : MonoBehaviour
{
    private static HandwriteRecognitionIOSPluguin _instance;

    public event Action<bool> OnCompleteInitialize;
    public event Action<string> OnCompleteGenerate;
    

#if UNITY_IOS
	// [DllImport("__Internal" )] static extern void _InitializeNew(string languageTag);
    
	// [DllImport("__Internal" )] static extern void _BeginTouchNew(float from, float to);
    
	// [DllImport("__Internal" )] static extern void _OnDragNew(float from, float to);
    
	// [DllImport("__Internal" )] static extern void _EndTouchNew(float from, float to);
    
	// [DllImport("__Internal" )] static extern void _GenerateNew();
	// [DllImport("__Internal" )] static extern void _ClearNew();
#endif

    public static HandwriteRecognitionIOSPluguin Instance
    {
        get
        {
            return _instance;
        }
    }

    public static void Initialize(string languageTag = "en-US")
    {
#if UNITY_IOS
        if (_instance != null) 
        {
            return;
        }
        Debug.Log("Initializing...");
        GameObject owner = new GameObject("HandwriteRecognitionIOSPluguin");
        _instance = owner.AddComponent<HandwriteRecognitionIOSPluguin>();

        // _InitializeNew(languageTag);
#endif
    }

    public void BeginTouch(PointerEventData eventData)
    {
#if UNITY_IOS
        // _BeginTouchNew(eventData.position.x, Screen.height - eventData.position.y);
#endif
    }

    public void OnDrag(PointerEventData eventData)
    {
#if UNITY_IOS
        // _OnDragNew(eventData.position.x, Screen.height - eventData.position.y);
#endif
    }

    public void EndTouch(PointerEventData eventData)
    {
#if UNITY_IOS
        // _EndTouchNew(eventData.position.x, Screen.height - eventData.position.y);
#endif
    }

    public void Generate(Action<string> callback)
    {
#if UNITY_IOS
        OnCompleteGenerate += callback;
        // _GenerateNew();
#endif
    }

    public void Clear()
    {
#if UNITY_IOS
        // _ClearNew();
#endif
    }



   
   public void InitializeCallback(string status)
   {
       OnCompleteInitialize?.Invoke(status == "1");
   }

   public void GenerateCallback(string result)
   {
       OnCompleteGenerate?.Invoke(result);
       OnCompleteGenerate = null;
   }
}
