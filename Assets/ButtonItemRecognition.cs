﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonItemRecognition : MonoBehaviour
{
    public static ButtonItemRecognition itemSelected;

    [SerializeField] EventTrigger eventTrigger;
    [SerializeField] Image image;

    public DemoV2Manager manager;

    public int position;
    public string result;

    private void Start()
    {
        if (eventTrigger == null) eventTrigger = GetComponent<EventTrigger>();
        if (image == null) image = GetComponent<Image>();

        eventTrigger.AddListener(EventTriggerType.PointerDown, FirstClick);
        eventTrigger.AddListener(EventTriggerType.EndDrag, EndDrag);
    }

    private void FirstClick(BaseEventData eventData)
    {
        manager.ResetRecognition();
        image.raycastTarget = false;
        itemSelected = this;
    }

    private void EndDrag(BaseEventData eventData)
    {
        manager.Read((result) =>
        {
            this.result = result;
            image.raycastTarget = true;
            itemSelected = null;
        });
    }
}
