// #import <GoogleMLKit/MLKit.h>

// @interface DigitalInkRecognition : NSObject

// @property(nonatomic) MLKDigitalInkRecognitionModel *model;
// @property(nonatomic) MLKModelManager *modelManager;
// @property(nonatomic) MLKDigitalInkRecognitionModelIdentifier *identifier;
// @property(weak, nullable, nonatomic) id delegate;

 
// @end

// @implementation DigitalInkRecognition : NSObject

// - (id)init
// {
//     self = [super init];
//     _modelManager = [MLKModelManager modelManager];
//     _delegate = self;
    
//     __weak __typeof(self) weakSelf = self;
//     [NSNotificationCenter.defaultCenter
//         addObserverForName:MLKModelDownloadDidSucceedNotification
//                     object:nil
//                      queue:NSOperationQueue.mainQueue
//                 usingBlock:^(NSNotification *notification) {
//                   __typeof(self) strongSelf = weakSelf;
//                   if (strongSelf == nil) {
//                     NSLog(@"self == nil handling download success notification");
//                     return;
//                   }
//                   if ([notification.userInfo[MLKModelDownloadUserInfoKeyRemoteModel]
//                           isEqual:strongSelf.model]) {
//                     [strongSelf.delegate displayMessage:@"Model download succeeded"];
//                   }
//                 }];
    
//     [NSNotificationCenter.defaultCenter
//         addObserverForName:MLKModelDownloadDidFailNotification
//                     object:nil
//                      queue:NSOperationQueue.mainQueue
//                 usingBlock:^(NSNotification *notification) {
//                   __typeof(self) strongSelf = weakSelf;
//                   if (strongSelf == nil) {
//                     NSLog(@"self == nil handling download fail notification");
//                     return;
//                   }
//                   if ([notification.userInfo[MLKModelDownloadUserInfoKeyRemoteModel]
//                           isEqual:strongSelf.model]) {
//                     [strongSelf.delegate displayMessage:@"Model download failed"];
//                   }
//                 }];
    
    
//     NSString *language = [[NSLocale preferredLanguages] firstObject];
    
//     self.identifier =
//         [MLKDigitalInkRecognitionModelIdentifier modelIdentifierFromLanguageTag:language error:nil];
//     [self displayMessage:language];
//     if (self.identifier == nil) {
//       self.identifier = [MLKDigitalInkRecognitionModelIdentifier modelIdentifierFromLanguageTag:@"en" error:nil];
//     }
    
    
//     NSLog(@"JALAN2");
    
    
//     return self;
// }
 
// - (IBAction)downloadModel {
    
//     NSLog(@"JALAN3");
    
//     if ([self.modelManager isModelDownloaded:self.model]) {
//       [self.delegate displayMessage:@"Model is already downloaded"];
//       return;
//     }
    
//     NSLog(@"JALAN4");
//     [self.delegate displayMessage:@"Starting download"];
//     // The NSProgress object returned by downloadModel: currently only takes on the values 0% or 100%
//     // so is not very useful. Instead we'll rely on the outcome listeners in the initializer to
//     // inform the user if a download succeeds or fails.
    
//     NSLog(@"JALAN6");
//     self.model = [[MLKDigitalInkRecognitionModel alloc] initWithModelIdentifier:self.identifier];
    
//     NSLog(@"JALAN7");
//     self.modelManager = [MLKModelManager modelManager];
//     NSLog(@"JALAN8");
    
//     [self.modelManager downloadModel:self.model
//                           conditions:[[MLKModelDownloadConditions alloc]
//                                        initWithAllowsCellularAccess:YES
//                                        allowsBackgroundDownloading:YES]];
    
//     NSLog(@"JALAN9");
    
// }

// - (void)displayMessage:(NSString *)message {
//     NSLog(@"LOG MESSAGE :: %@", message);
// //  self.messageLabel.text = message;
// }

// @end


// static DigitalInkRecognition* recognition = nil;

// extern "C" void _InitializeNew(const char* languageTag)
// {
//     NSLog(@"JALAN1");

//     if (recognition == nil)
//         recognition = [[DigitalInkRecognition alloc] init];

//     [recognition downloadModel];
// }


// extern "C" void _BeginTouchNew(float xPoint, float yPoint)
// {
//     CGPoint point = CGPointMake(CGFloat(xPoint), CGFloat(yPoint));
// //    [viewController onBeginTouch:point];
// }

// extern "C" void _OnDragNew(float xPoint, float yPoint)
// {
//     CGPoint point = CGPointMake(CGFloat(xPoint), CGFloat(yPoint));
// //    [viewController onDragTouch:point];
// }

// extern "C" void _EndTouchNew(float xPoint, float yPoint)
// {
//     CGPoint point = CGPointMake(CGFloat(xPoint), CGFloat(yPoint));
// //    [viewController onEndTouch:point];
// }

// extern "C" void _GenerateNew()
// {
// //    [viewController didPressRecognize];
// }

// extern "C" void _ClearNew()
// {
// //    [viewController didPressClear];
// }
